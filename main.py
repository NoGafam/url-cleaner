import subprocess
from flask import Flask, jsonify, request
from pathlib import Path

app = Flask(__name__)


@app.route('/api', methods=['GET'])
def mon_api():
    raw_url = request.args.get('raw_url')

    message = clear_url(raw_url)
    return jsonify({
        'status': 'success',
        'message': message
    }), 200


def clear_url(url):
    p = Path(__file__).with_name('clean_url.sh')
    filename = p.absolute()
    redirection = subprocess.check_output([filename, url])
    return redirection.decode('utf-8')

if __name__ == '__main__':
    app.run(debug=True)
