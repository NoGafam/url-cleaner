[🇬🇧 English Version](media/README.md)

# Présentation

**URL Cleanner** expose une API REST (GET) qui extrait la vraie URL d'un article et renvoie un lien sans traqueurs en utilisant private-reader.


Voici un aperçu du résultat :

![screenshot](media/screenshot.png)

Pour l'utiliser vous devez fournir une URL en argument `raw_url` d'une requête GET.