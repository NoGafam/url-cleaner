# Presentation

**URL Cleanner** exposes a REST API (GET) that extracts the true URL of an article and returns a tracker-free link using private-reader.

Here's a look at the result:

![screenshot](screenshot.png)

To use it, you need to supply a URL as the `raw_url` argument to a GET request.